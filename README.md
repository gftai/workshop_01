# GFT AI Workshop

![LOGO](https://raw.githubusercontent.com/gft-academy-pl/gcp-data-analysis-with-bigquery/master/assets/gft-academy.png)

## Prerequisites

* Active Google Cloud Platform account: https://cloud.google.com/free/ (you need to attach Credit Card in order to use trial period)
* Some Python, SQL and shell scripting knowledge
* Own laptop with:
  * wi-fi (access will be provided)
  * newest Chrome browser

## Plan

### [Step 0 - Init](./00-init.md)

### [Step 1 - Datalab](./01-datalab.md)

### [Step 2 - Machine Learning with Google Cloud Platform](./02-GCP.md)

### Step 3 - Introduction to Pandas

### Step 4 - The workbook!

