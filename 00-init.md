![GCP Logo](https://raw.githubusercontent.com/gft-academy-pl/gcp-data-analysis-with-bigquery/master/assets/google-cloud-platform.png)

## Agenda

- Introduction to GCP
- Introduction to GCP shell and editor
- Initialization / setup

## Introduction to GCP

Google Cloud Platform is a public cloud solution providing a set of product: compute, storage, database cloud AI, networking, identity & security. 

- https://cloud.google.com/ 
- http://console.cloud.google.com

## Introduction to GCP shell and editor

### Cloud shell

Google Cloud Shell provides you with command-line access to your cloud resources directly from your browser. You can easily manage your projects and resources without having to install the Google Cloud SDK or other tools on your system. With Cloud Shell, the Cloud SDK gcloud command-line tool and other utilities you need are always available, up to date and fully authenticated.

When you start Cloud Shell, g1-small Google Compute Engine virtual machine running a Debian-based Linux operating system is provisioned for you (for free!). Cloud Shell instances are provisioned on a per-user, per-session basis. The instance persists while your Cloud Shell session is active and terminates after an hour of inactivity. 

**Remember:** *Each time Cloud Shell is restarted, you are loosing all of variables exported by you (until you put them into ~/.bashrc or ~/.bash_profile)!*

## Initialization / setup

#### Open Google Cloud Console
Make sure that you are using Chrome. Open new tab/window in the browser.
Go to https://console.cloud.google.com/ (log in using your credentials if required)

#### Create dedicated project

In order to easily clean resources, create dedicated project named ai-workshop-<your initials> and switch to it after creation.

Scripts often use project-id instead of project name. You can find it using below script. Open Cloud Shell using button in top right corner of the screen. Execute command and make sure that it displays newly created project id.
```
echo ${GOOGLE_CLOUD_PROJECT} 
```
#### Optional - Enable billing in the project
When you have multiple billing accounts, link newly created project-id to respective billing account. Get ID of your billing acount from the list of all you can access:
```
gcloud alpha billing accounts list
```

Enable billing in the project
```
gcloud alpha billing projects link ${GOOGLE_CLOUD_PROJECT} --billing-account ACCOUNT_ID-of-your-preferred-billing-account
```

#### Enable Required APIs
Execute below commands in Cloud Shell:
```
gcloud services enable compute.googleapis.com
gcloud services enable sourcerepo.googleapis.com
```

#### Cloud shell editor

```
cloudshell edit .
```

#### Clean up

After the workshop is over, please delete the project as your account will be still billed for the VM and bucket usage.
Go to **IAM & admin** - **Manage resources**, select ai-workshop-<your initials> project and click **Delete** button.



## Navigation

- [Next Step](./01-datalab.md)
