## Agenda
- Datalab overview
- Key concepts & components
- Pricing
- Cloud Datalab setup
- Cloud Source Repository for Datalab
- Troubleshooting
- Managing your VM instances

![Diagram](https://bitbucket.org/gftai/workshop_01/raw/master/HousePricingChallenge/img/datalab-integrated.svg)

## Datalab overview
Cloud Datalab is a powerful interactive tool created to explore, analyze, transform and visualize data and build machine learning models on Google Cloud Platform. It runs on Google Compute Engine and connects to multiple cloud services easily so you can focus on your data science tasks.
## Key concepts & components
Cloud Datalab is packaged as a container and run in a VM (Virtual Machine) instance.

Cloud Datalab is built on Jupyter Notebook (formerly IPython). Notebooks are documents which contain both computer code (Python, SQL, and JavaScript), documentation written as markdown, and the results of code execution (text, image or, HTML/JavaScript). This way Notebooks contain analysis description, the results (figures, tables, etc..) as well as executable documents which can be run to perform data analysis.

Like a code editor or IDE, notebooks help you write code: they allow you to execute code in an interactive and iterative manner, rendering the results alongside the code. Further, when you share a notebook with team members, you can include code, markdown-formatted documentation, and results that include interactive charts, to provide them with context that goes beyond what Python or SQL code files alone can provide.

When you open a notebook, a backend “kernel” process is launched to manage the variables defined during the session and execute your notebook code. When the executed code accesses Google Cloud services such as BigQuery or Google Machine Learning Engine, it uses the service account available in the VM. Hence, the service account must be authorized to access the data or request the service. 

Datalab notebooks simplifies data processing with Cloud BigQuery, Cloud Machine Learning Engine, Cloud Storage, and Stackdriver Monitoring. Authentication, cloud computation and source control are taken care of out-of-the-box.


## Why GFT Academy on Datalab ?

### Unified development environment 
DataLab is built and packaged as a docker container image which can be deployed in no time.

### Pre-installed packages
Cloud Datalab includes a set of [libraries](https://cloud.google.com/datalab/docs/concepts/key-concepts#included_libraries). The included libraries are intended to support common data analysis, transformation, and visualization scenarios, including:
```
matplotlib at version 1.5.3
numpy at version 1.11.2
pandas at version 0.19.1
plotly at version 1.12.5
scikit-image at version 0.13.0
scikit-learn at version 0.18.2
seaborn at version 0.7.0
tensorflow at version 1.5
```

### Scaling on GCE
Training models on Google Datalab is a simple way to scale. This method gives you the advantage of:

* Vertical scaling by using an arbitrarily powerful GCE instance: up to 8 Cores and 52GB RAM.
* GPU speedup by using a GCE instance with Tesla GPU enabled (not possible for scikit-learn, but supported by xgboost and h2o).

## Pricing
There is no charge for using Google Cloud Datalab. However, you do pay for any Google Cloud Platform resources you use with Cloud Datalab, for example:
* Compute resources: You incur costs of Datalab VM machine (default type type is n1-standard-1). You can minimize costs by stopping VM instances when you are not using them. 
```
datalab stop instance-name
```
* Storage resources: You are also charged for a 20GB VM Boot Disk +  200GB Standard Persistent Disk, where user notebooks are stored. 200GB disk remains after the deletion of the VM until you delete it. The following command deletes the VM instance and 20GB boot disk as well as the 200GB user notebook disk.
```
datalab delete --delete-disk instance-name
```
* Data Analysis Services: You incur Google BigQuery costs when issuing SQL queries. Also, when you use Google Cloud Machine Learning, you may incur Cloud Machine Learning Engine and/or Google Cloud Dataflow charges.
* Other resources: You may incur costs for other API requests you make within the Cloud Datalab notebook environment. 

## Cloud Datalab setup
1) Cloud Datalab can be created either using Cloud SDK on your local machine or using [Cloud Shell](https://cloud.google.com/shell/docs/starting-cloud-shell#starting_a_new_session) from Google Cloud Console. Run the following command to create a new Cloud Datalab VM instance:
```
datalab create --zone europe-west4-a aiworkshop
```
2) Confirm that you want to continue when prompted.

3) Enter rsa passphrase (can be empty).

4) Wait for operation to complete (It may take several minutes to propagate shh keys). Once completed, Datalab connection will be automatically opened. If you loose connection, run following command to connect to a existing Cloud Datalab VM instance:

```
 datalab connect --zone europe-west4-a --port 8081 aiworkshop
```
6) Open your browser to the Cloud Datalab home page by clicking the Web preview button ![Diagram](https://bitbucket.org/gftai/workshop_01/raw/master/HousePricingChallenge/img/web-preview-button.png), and then selecting Change port→Port 8081.

![Diagram](https://bitbucket.org/gftai/workshop_01/raw/master/HousePricingChallenge/img/web-preview.png)

There is more convenient way to do that - scroll up several lines, up to 'This tool needs to create the directory...' (just before you confirmed you want to continue), one line up is clickable link 'http://localhost:8081' - press it and new tab with fancy URL is opened.

7) Go to 'notebooks' directory

8) Download below files to your local machine (right-click and Save link as):

[workshop_utils.py](https://bitbucket.org/gftai/workshop_01/downloads/workshop_utils.py)

[gft-academy_lodz.ipynb](https://bitbucket.org/gftai/workshop_01/downloads/gft-academy_lodz.ipynb)

[DatalabAndPandasIntro.ipynb](https://bitbucket.org/gftai/workshop_01/downloads/DatalabAndPandasIntro.ipynb)

9) In Datalab notebook click **Upload** button and upload all files.

## Troubleshooting
To stop the Cloud Datalab VM, click the account icon ![Diagram](https://bitbucket.org/gftai/workshop_01/raw/master/HousePricingChallenge/img/user-icon.png) in the top-right corner of the Cloud Datalab notebook, then click **Stop VM** button (VM will be automatically stopped after 90 minutes of inactivity). 

![Diagram](https://bitbucket.org/gftai/workshop_01/raw/master/HousePricingChallenge/img/datalab-server-vm-ui-about.png)

When you install new Python library, kernel restart is required: in above menu select **About Datalab**, then click the **Restart Server** option from the *About Google Cloud Datalab* dialog.

![Diagram](https://bitbucket.org/gftai/workshop_01/raw/master/HousePricingChallenge/img/restart-server.png)

## Managing your VM instances
Go to [Cloud Console](https://console.cloud.google.com/compute/instances) and open **Compute Engine** - ** VM Instances**

## Navigation

- [Previous Step](./00-init.md)
- [Next Step](./02-GCP.md)