## BigQuery - highly scalable data warehouse
Google BigQuery is a highly scalable data warehouse and massive data storage system, which allocates storage and query resources dynamically based on usage patterns. Runs SQL queries on gigabytes to petabytes of data and dynamically assigns up to 2000 nodes. 

BigQuery ML enables users to create and execute machine learning models in BigQuery using standard SQL queries. BigQuery ML increases development speed by eliminating the need to move data.

BigQuery ML currently supports the following types of models:
* Linear regression 
* Binary logistic regression
* Multiclass logistic regression for classification

![BigQuery](https://bitbucket.org/gftai/workshop_01/raw/master/HousePricingChallenge/img/dremel.png)

## Dataprep - interactive data profiling
Google Cloud Dataprep surfaces visual representations of your data for individual columns and the entire dataset. These visual profiles allow you to make quick assessments of problems, unusual patterns, and required changes to your data, and they are available throughout the development of your dataset.

Cloud Dataprep is built on top of the powerful Cloud Dataflow service. Cloud Dataprep is auto-scalable and can easily handle processing massive data sets.

Supports common data sources of any size: megabytes to terabytes, structured and unstructured. Transform data stored in CSV, JSON, or relational table formats, processes data stored in Cloud Storage, BigQuery, or from your desktop.

![Datapre](https://bitbucket.org/gftai/workshop_01/raw/master/HousePricingChallenge/img/dataprep_aggregating-column-example.png)

- [Previous Step](./01-datalab.md)